import express from 'express';
import bodyParser from 'body-parser';
import fs from 'fs';

var app = express();

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.get('/user', (req, res) => {
  fileReader('./users.json', (err, users) => {
    if (err) {
      console.log(err)
      return
    }
  return res.status(200).send({
    success: 'true',
    message: 'users retrieved successfully',
    users: users
    })
  })
});

app.post('/user', (req, res) => {
  if(!req.body.name) {
    return res.status(400).send({
      success: 'false',
      message: 'name is required'
    });
  } else if(!req.body.health) {
    return res.status(400).send({
      success: 'false',
      message: 'health parameter is required'
    });
  } else if(!req.body.attack) {
    return res.status(400).send({
      success: 'false',
      message: 'attack parameter is required'
    });
  } else if(!req.body.defense) {
    return res.status(400).send({
      success: 'false',
      message: 'defense parameter is required'
    });
  } else if(!req.body.source) {
    return res.status(400).send({
      success: 'false',
      message: 'source is required'
    });
  }
 const user = {
   _id: db.length + 1,
   name: req.body.name,
   health: req.body.health,
   attack: req.body.attack,
   defense: req.body.defense,
   source: req.body.source,
 }

 fileReader('./users.json', (err, users) => {
  if (err) {
      console.log(err)
      return
  }
  console.log('users', users)
  users.push(user);

  const jsonString = JSON.stringify(users);
  fs.writeFileSync('./users.json', jsonString);
 })


 return res.status(201).send({
   success: 'true',
   message: 'user added successfully',
   user
 })
});

app.get('/user/:id', (req, res) => {

  const id = parseInt(req.params.id, 10);

  fileReader('./users.json', (err, users) => {
    if (err) {
        console.log(err)
        return
      }
  
    users.map((user) => {
      if (user._id === id) {
        return res.status(200).send({
          success: 'true',
          message: 'user retrieved successfully',
          user,
        });
      } 
    });
    return res.status(404).send({
      success: 'false',
      message: 'user does not exist',
    });
  });
});

app.delete('/user/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);

  fileReader('./users.json', (err, users) => {
    if (err) {
        console.log(err)
        return
      }
  
    users.map((user, index) => {
      if (user._id === id) {
        users.splice(index, 1);
        return res.status(200).send({
          success: 'true',
          message: 'user deleted successfuly',
        });
      }
    });
    const jsonString = JSON.stringify(users);
    fs.writeFileSync('./users.json', jsonString);

    return res.status(404).send({
      success: 'false',
      message: 'user not found',
    });
  });
});

app.put('/user/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);
  let userFound;
  let userIndex;

  db.map((user, index) => {
    if (user._id == id) {
      userFound = user;
      userIndex = index;
    }
  });

  fileReader('./users.json', (err, users) => {
    if (err) {
        console.log(err)
        return
      }
  
    users.map((user, index) => {
      if (user._id == id) {
        userFound = user;
        userIndex = index;
      }
    });

    if (!userFound) {
      return res.status(404).send({
        success: 'false',
        message: 'user not found',
      });
    }

    if(!req.body.name) {
      return res.status(400).send({
        success: 'false',
        message: 'name is required'
      });
    } else if(!req.body.health) {
      return res.status(400).send({
        success: 'false',
        message: 'health parameter is required'
      });
    } else if(!req.body.attack) {
      return res.status(400).send({
        success: 'false',
        message: 'attack parameter is required'
      });
    } else if(!req.body.defense) {
      return res.status(400).send({
        success: 'false',
        message: 'defense parameter is required'
      });
    } else if(!req.body.source) {
      return res.status(400).send({
        success: 'false',
        message: 'source is required'
      });
    }

    const updatedUser = {
      _id: userFound._id,
      name: req.body.name || userFound.title,
      health: req.body.health || userFound.health,
      attack: req.body.attack || userFound.attack,
      defense: req.body.defense || userFound.defense,
      source: req.body.source || userFound.source,
    };

    users.splice(userIndex, 1, updatedUser);

    const jsonString = JSON.stringify(users);
    fs.writeFileSync('./users.json', jsonString);

    return res.status(201).send({
      success: 'true',
      message: 'user added successfully',
      updatedUser,
    });
  });
});

app.listen(3000);

function fileReader(filePath, cb) {
  fs.readFile(filePath, (err, fileData) => {
      if (err) {
          return cb && cb(err)
      }
      try {
          const object = JSON.parse(fileData)
          return cb && cb(null, object)
      } catch(err) {
          return cb && cb(err)
      }
  })
}


# Task: Express yourself with Node.js 



## Table of contents


- Getting started
- Routes
 

## Getting started

Clone repository

```
git clone https://AntonShevtsov@bitbucket.org/AntonShevtsov/stage-2-express-yourself-with-nodejs.git
```

Install dependencies
```
npm install
```

Start  Server

```bash
npm start
```

Now if you go to [http://localhost:3000/user/1](http://localhost:3000/user/1), you'll get

```json
{ "_id": 1, "name": "Ryu", "health": 45, "attack": 4, "defense": 3, "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/ryu-hdstance.gif" }
```

Also when doing requests, it's good to know that:

- If you make POST, PUT,  or DELETE requests, changes will be automatically and safely saved to file `users.json`.
- Your request body JSON should be object enclosed, just like the GET output. (for example `{"name": "Foobar"}`)

## Routes

Here are all the  routes available at the moment.


```
GET    /user
GET    /user/1
POST   /user
PUT    /user/1
DELETE /user/1
```

## License

MIT
